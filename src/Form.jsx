import { useForm } from 'react-hook-form'
import './Form.css';
const Form = () => {
const {register,handleSubmit,formState:{errors},reset,trigger}=useForm({defaultValues:{
        FastName:'',
        LastName:'',
        Email:'',
        phoneNumber:'',
        age:'',
        Title:'',
        password:'',
    }});
    console.log(errors)
    const onSubmit =(data)=>{
      console.log(data)
      reset()
    }
  return (
    <>
        <div className='sub-form'>
        <form onSubmit={handleSubmit(onSubmit)}>
         <div className='Email'>
          <label>Email:</label>
         <input name='Email'{...register('Email',{required:true,pattern: {value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,message: "Invalid email address"}})}       onClick=  {() => {trigger("Email")}} placeholder='Email'></input>
         {errors.Email && ( <p style={{color:'red'}}>{errors.Email.message}</p>)}
         </div>
         
         <div className='FirstName'>
         <label>FirstName: </label>
         <input name='FirstName' {...register ('FastName',{required:true})}  placeholder='FastName'></input>
         <p style={{color:'red'}}>{errors.FastName?.type === 'required' && 'FastName is required '}</p>
         </div>

         <div className='LastName'>
         <label>LastName: </label>
         <input name='LastName' {...register ('LastName',{required:true})}  placeholder='LastName'></input>
         <p style={{color:'red'}}>{errors.LastName?.type === 'required' && 'LastName is required '}</p>
         </div>

         <div className='PhoneNumber'>
         <label>PhoneNumber: </label>
         <input name='PhoneNumber' {...register ('phoneNumber',{required:true})}  placeholder='PhoneNumber'></input>
         <p style={{color:'red'}}>{errors.phoneNumber?.type === 'required' && 'PhoneNumber is required'}</p>
         </div>

         <div className='Age'>
         <label>Age: </label>
         <input name ='Age' type='number' {...register('Age', {min:{value:18 ,message:'minimum required age is 18'}, max:{value:60,message:'maxmum reuired age is 60'}})} onClick={()=>{trigger('age')}} placeholder='Age'/>
         <p style={{color:'red'}}>{errors.age && (errors.age.message)}</p>
         </div>

       
         <div className='Gender'>
         <label>Gender: </label>
         <input name='Male' {...register('checkbox',{required:true})} type='radio' value='Male' />
         <label>Male</label>
         <input name='Female' {...register('checkbox',{required:true})} 
         type='radio' value='Female' />
         <label>Female</label>
         

         <div className='Type'>
         <label>Type:</label>
         <select name='Title' {...register ('Title', {required:true})}>
         <option value='Student'>student</option>
         <option value='Employee'>Employee</option>
         </select>
         </div>
         </div>


         <div className='Password'>
         <label>Password:</label>
         <input name='password' {...register('password',{required:true})} placeholder='Password'></input>
         <p style={{color:'red'}}>{errors.password?.type === 'required' && 'password is required'}</p>
         <input type='submit'></input> 
         </div>
    </form>
        </div>
    </>
  )
}

export default Form